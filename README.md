哎嘛-OSCHINA第三方客户端
===========

本项目采用 GPL 授权协议，欢迎大家在这个基础上进行改进，并与大家分享。

如您感觉本项目中有不妥之处或者有不爽的地方，欢迎提交问题或更改方案，项目小组会
及时的对您提交的修改给予反馈。希望能为开发者提供一款开源好用的Android版客户端产品。
一款好产品需要大家共同努力，大家共勉！

# **项目简要说明** #

*注：本文假设你已经有Android开发环境*

本项目采用Android Studio开发工具开发，如果你安装有Android Studio可以直接导入
项目后进行二次开发。

